package com.ayge.registro

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_listar_peliculas.*

class ListarPeliculasActivity : AppCompatActivity() {

    val Adaptador : AdaptadorRecycler = AdaptadorRecycler()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_listar_peliculas)

        rv_listar_peliculas.setHasFixedSize(true)
        rv_listar_peliculas.layoutManager = LinearLayoutManager(this)
        Adaptador.RecyclerAdapter(getPelicula(), this)
        rv_listar_peliculas.adapter = Adaptador
    }

        fun getPelicula(): MutableList<Pelicula> {
            var peliculas:MutableList<Pelicula> = ArrayList()
            peliculas.add(Pelicula(1, "Los Increibles", "Español", "increibles"))
            peliculas.add(Pelicula(2, "Mary Poppins 2", "Español", "mary_poppins"))
            peliculas.add(Pelicula(3, "La vida secreta de mis mascotas 2", "Español", "mascotas2"))
            peliculas.add(Pelicula(4, "Mi villano Favorito 3", "Español", "villano"))
            return peliculas
        }


    }
