package com.ayge.registro

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_pelicula.view.*

class AdaptadorRecycler : RecyclerView.Adapter<AdaptadorRecycler.ViewHolder>() {

    var peliculas: MutableList<Pelicula>  = ArrayList()
    lateinit var context: Context

    fun RecyclerAdapter(pelicula : MutableList<Pelicula>, context: Context){
        this.peliculas = pelicula
        this.context = context
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = peliculas.get(position)
        holder.bind(item, context)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_pelicula,parent,false))

    }

    override fun getItemCount(): Int {
        return peliculas.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val iv_pelicula = view.iv_rv_imagen
        val tv_titulo = view.tv_rv_pelicula
        val tv_idioma = view.tv_rv_idioma
        val tv_id = view.tv_id

        fun bind(pelicula:Pelicula, context: Context){
            var id = pelicula.id
            when(id){
                1 -> {iv_pelicula.setImageResource(R.drawable.increibles)}
                2 -> {iv_pelicula.setImageResource(R.drawable.mary_poppins)}
                3 -> {iv_pelicula.setImageResource(R.drawable.mascotas2)}
                4 -> {iv_pelicula.setImageResource(R.drawable.villano)}
            }
            tv_titulo?.text=pelicula.pelicula
            tv_idioma?.text=pelicula.idioma

            itemView.setOnClickListener(View.OnClickListener {
                val detalleIntent = Intent(context, DetallesPeliculaActivity::class.java)
                detalleIntent.putExtra("pelicula", pelicula)
                itemView.context.startActivity(detalleIntent)
                Toast.makeText(context, "pelicula seleccionada "+tv_titulo.text, Toast.LENGTH_SHORT).show()

            })
        }
    }
}
