package com.ayge.registro

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.CompoundButton
import android.widget.Toast

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

class RegistroActivity : AppCompatActivity() {

    val diccionario:HashMap<String,String> = HashMap<String,String>()
    lateinit var pelicula:Pelicula
    var factura = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        pelicula = intent.extras?.getSerializable(("pelicula")) as Pelicula
        val id = pelicula.id;

        when(id){
            1 -> {iv_pelicula.setImageResource(R.drawable.increibles)}
            2 -> {iv_pelicula.setImageResource(R.drawable.mary_poppins)}
            3 -> {iv_pelicula.setImageResource(R.drawable.mascotas2)}
            4 -> {iv_pelicula.setImageResource(R.drawable.villano)}
        }

        val idiomas = arrayListOf<String>("Español", "Subtitulado", "Original")
        val idiomaAdapter = ArrayAdapter<String>(baseContext,android.R.layout.simple_spinner_item, idiomas)
        idiomaAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item) // Ocupa una vista por default
        sp_idiomas.adapter=idiomaAdapter

        sp_idiomas.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                val idioma = idiomas[position]
                diccionario.put("idioma", idioma)
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {

            }
        })

        var tipoCliente = "Estandar"
        rg_tipo_cliente.setOnCheckedChangeListener{group, checkedId ->
            when(checkedId){
                rb_normal.id ->
                    tipoCliente = "Estandar"
                rb_premium.id ->
                    tipoCliente = "Premium"
            }
        }


        chb_factura.setOnCheckedChangeListener({ buttonView, isChecked ->
                factura = isChecked })

        ib_trailer.setOnClickListener(View.OnClickListener {
            val id = "6RBr1v9_umA"
            val intentApp = Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:$id"))
            val intentNavegador = Intent(
                Intent.ACTION_VIEW,
                Uri.parse("http://www.youtube.com/watch?v=$id")
            )
            try {
                startActivity(intentApp)
            } catch (ex: ActivityNotFoundException) {
                startActivity(intentNavegador)
            }

        })


        til_nombre.editText?.setOnFocusChangeListener(View.OnFocusChangeListener { view, hasFocus ->
            val nombre=til_nombre.editText?.text.toString()

            if (!hasFocus){
                if (nombre.isNotEmpty()) {
                    til_nombre.isErrorEnabled = false
                }else {
                    til_nombre.isErrorEnabled = true
                    til_nombre.error = "El nombre no puede quedar vacio"
                }
            }
        })

        til_apellidos.editText?.setOnFocusChangeListener(View.OnFocusChangeListener { view, hasFocus ->
            val apellidos=til_apellidos.editText?.text.toString()

            if (!hasFocus){
                if (apellidos.isNotEmpty()) {
                    til_apellidos.isErrorEnabled = false
                }else {
                    til_apellidos.isErrorEnabled = true
                    til_apellidos.error = "Los apellidos no pueden ser vacios"
                }
            }
        })

        til_direccion.editText?.setOnFocusChangeListener(View.OnFocusChangeListener { view, hasFocus ->
            val direccion=til_direccion.editText?.text.toString()
            if (!hasFocus){
                if (direccion.isNotEmpty()) {
                    til_direccion.isErrorEnabled = false
                }else {
                    til_direccion.isErrorEnabled = true
                    til_direccion.error = "La direccion no puede ser vacia"
                }
            }
        })

        fab_pagar.setOnClickListener { view ->
            if(til_nombre.editText?.text.toString().isEmpty() || til_apellidos.editText?.text.toString().isEmpty() || til_direccion.editText?.text.toString().isEmpty()){
                Toast.makeText(baseContext,"Debe llenar todos los campos", Toast.LENGTH_LONG).show()
            }else {
                if (!til_nombre.isErrorEnabled)
                    diccionario.put("nombre", til_nombre.editText?.text.toString())
                if (!til_apellidos.isErrorEnabled)
                    diccionario.put("apellidos", til_apellidos.editText?.text.toString())
                if (!til_direccion.isErrorEnabled)
                    diccionario.put("direccion", til_direccion.editText?.text.toString())
                diccionario.put("cliente", tipoCliente)
                if (factura) {
                    diccionario.put("factura", "Si")
                } else {
                    diccionario.put("factura", "No")
                }

                val intentDetalles = Intent(this, DetallesActivity::class.java)
                intentDetalles.putExtra("diccionario", diccionario)
                startActivity(intentDetalles)
            }


        }
    }


}
