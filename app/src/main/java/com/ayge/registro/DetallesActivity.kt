package com.ayge.registro

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_detalles.*

class DetallesActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detalles)

        val diccionario:HashMap<String,String>  = intent.extras?.getSerializable("diccionario") as HashMap<String,String>

        val nombre = diccionario.get("nombre")
        val apellidos = diccionario.get("apellidos")
        val direccion = diccionario.get("direccion")
        val idioma = diccionario.get("idioma")
        val cliente = diccionario.get("cliente")
        val factura = diccionario.get("factura")



        detallesNombre.setText("Nombre: ${nombre} ${apellidos}")
        detallesDireccion.setText("Direccion: ${direccion}")
        detallesTipoCliente.setText("Tipo de cliente: ${cliente}")
        detallesIdioma.setText("Idioma: ${idioma}")
        detallesFactura.setText("Requiere factura: ${factura}")

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_regresar, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_listar -> {

                val intentListar = Intent(this, ListarPeliculasActivity::class.java)
                startActivity(intentListar)

                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
