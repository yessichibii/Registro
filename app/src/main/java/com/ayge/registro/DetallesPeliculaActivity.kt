package com.ayge.registro

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_detalles_pelicula.*

class DetallesPeliculaActivity : AppCompatActivity() {

    lateinit var pelicula:Pelicula


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detalles_pelicula)

        pelicula = intent.extras?.getSerializable(("pelicula")) as Pelicula

        val idioma = pelicula.idioma
        val titulo = pelicula.pelicula
        val id = pelicula.id;

        when(id){
            1 -> {iv_det_imagen.setImageResource(R.drawable.increibles)}
            2 -> {iv_det_imagen.setImageResource(R.drawable.mary_poppins)}
            3 -> {iv_det_imagen.setImageResource(R.drawable.mascotas2)}
            4 -> {iv_det_imagen.setImageResource(R.drawable.villano)}
        }
        tv_det_idioma?.text = idioma
        tv_det_titulo?.text = titulo

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_registrar -> {
                val intentRegistro = Intent(this, RegistroActivity::class.java)
                intentRegistro.putExtra("pelicula", pelicula)
                startActivity(intentRegistro)
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
